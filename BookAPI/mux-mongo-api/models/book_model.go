package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Book struct {
	IdBook      primitive.ObjectID `json:"id,omitempty"`
	Title       string             `json:"title,omitempty" validate:"required"`
	Price       int                `json:"price,omitempty" validate:"required"`
	Description string             `json:"description,omitempty" validate:"required"`
	Author      string             `json:"author,omitempty" validate:"required"`
	Store       string             `json:"store,omitempty" validate:"required"`
}
