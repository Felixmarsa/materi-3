package routes

import (
	"mux-mongo-api/controllers" //add this

	"github.com/gorilla/mux"
)

func BookRoute(router *mux.Router) {
	router.HandleFunc("/book", controllers.CreateBook()).Methods("POST")             //add this
	router.HandleFunc("/book/{bookId}", controllers.GetABook()).Methods("GET")       //add this
	router.HandleFunc("/book/{bookId}", controllers.EditABook()).Methods("PUT")      //add this
	router.HandleFunc("/book/{bookId}", controllers.DeleteABook()).Methods("DELETE") //add this
	router.HandleFunc("/books", controllers.GetAllBook()).Methods("GET")             //add this
}
