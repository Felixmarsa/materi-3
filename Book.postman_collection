{
	"info": {
		"_postman_id": "e14422a3-cf10-4430-89c9-fd151206dbab",
		"name": "Simple Book API",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
		"_exporter_id": "21784389"
	},
	"item": [
		{
			"name": "API Status",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 200\", () => {\r",
							"    pm.response.to.have.status(200);\r",
							"});\r",
							"\r",
							"const response = pm.response.json();\r",
							"\r",
							"console.log(response.status);\r",
							"console.log(response['status']);\r",
							"\r",
							"pm.test(\"Status should be OK\", () => {\r",
							"    pm.expect(response.status).to.eql(\"OK\");\r",
							"});\r",
							"\r",
							"postman.setNextRequest(\"List of books\");"
						],
						"type": "text/javascript"
					}
				}
			],
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "{{baseUrl}}/status",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"status"
					]
				}
			},
			"response": []
		},
		{
			"name": "Register API Client",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 201\", () => {\r",
							"    pm.response.to.have.status(201);\r",
							"});"
						],
						"type": "text/javascript"
					}
				}
			],
			"request": {
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n   \"clientName\": \"felixM\",\r\n   \"clientEmail\": \"felixM@example.com\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "{{baseUrl}}/api-clients",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"api-clients"
					]
				}
			},
			"response": []
		},
		{
			"name": "List of books",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 200\", () => {\r",
							"    pm.response.to.have.status(200);\r",
							"});\r",
							"\r",
							"const response = pm.response.json();\r",
							"\r",
							"const nonFictionBooks = response.filter((book) => book.available === true);\r",
							"\r",
							"const book = nonFictionBooks[0];\r",
							"\r",
							"if(book){\r",
							"    pm.globals.set(\"bookId\", book.id);\r",
							"}\r",
							"\r",
							"pm.test(\"Book found\", () => {\r",
							"    pm.expect(book).to.be.an('object');\r",
							"    pm.expect(book.available).to.be.true;\r",
							"    pm.expect(book.available).to.eql(true);\r",
							"    \r",
							"}); \r",
							"\r",
							"pm.globals.set(\"bookId\", nonFictionBooks[0].id);"
						],
						"type": "text/javascript"
					}
				}
			],
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "{{baseUrl}}/books?type =non-fiction",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"books"
					],
					"query": [
						{
							"key": "type ",
							"value": "non-fiction"
						}
					]
				}
			},
			"response": []
		},
		{
			"name": "Get single book",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 200\", () => {\r",
							"    pm.response.to.have.status(200);\r",
							"});\r",
							"\r",
							"const response = pm.response.json();\r",
							"\r",
							"pm.test(\"Is in stock\", () => {\r",
							"    pm.expect(response['current-stock']).to.be.above(2);\r",
							"});"
						],
						"type": "text/javascript"
					}
				}
			],
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "{{baseUrl}}/books/:id",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"books",
						":id"
					],
					"variable": [
						{
							"key": "id",
							"value": "{{bookId}}"
						}
					]
				}
			},
			"response": []
		},
		{
			"name": "Order book",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 201\", () => {\r",
							"    pm.response.to.have.status(201);\r",
							"});\r",
							"\r",
							"const response = pm.response.json();\r",
							"\r",
							"pm.globals.set(\"orderId\", response.orderId);"
						],
						"type": "text/javascript"
					}
				}
			],
			"request": {
				"auth": {
					"type": "bearer",
					"bearer": [
						{
							"key": "token",
							"value": "{{accessToken}}",
							"type": "string"
						}
					]
				},
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n  \"bookId\": {{bookId}},\r\n  \"customerName\": \"{{$randomFullName}}\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "{{baseUrl}}/orders",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"orders"
					]
				}
			},
			"response": []
		},
		{
			"name": "Get all book orders",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 200\", () => {\r",
							"    pm.response.to.have.status(200);\r",
							"});"
						],
						"type": "text/javascript"
					}
				}
			],
			"protocolProfileBehavior": {
				"disableBodyPruning": true
			},
			"request": {
				"auth": {
					"type": "bearer",
					"bearer": [
						{
							"key": "token",
							"value": "{{accessToken}}",
							"type": "string"
						}
					]
				},
				"method": "GET",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n  \"bookId\": 1,\r\n  \"customerName\": \"{{$randomFullName}}\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "{{baseUrl}}/orders",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"orders"
					]
				}
			},
			"response": []
		},
		{
			"name": "Get an order",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 200\", () => {\r",
							"    pm.response.to.have.status(200);\r",
							"});"
						],
						"type": "text/javascript"
					}
				}
			],
			"protocolProfileBehavior": {
				"disableBodyPruning": true
			},
			"request": {
				"auth": {
					"type": "bearer",
					"bearer": [
						{
							"key": "token",
							"value": "{{accessToken}}",
							"type": "string"
						}
					]
				},
				"method": "GET",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n  \"bookId\": 1,\r\n  \"customerName\": \"{{$randomFullName}}\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "{{baseUrl}}/orders/:orderId",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"orders",
						":orderId"
					],
					"variable": [
						{
							"key": "orderId",
							"value": "{{orderId}}"
						}
					]
				}
			},
			"response": []
		},
		{
			"name": "Update an order",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 204\", () => {\r",
							"    pm.response.to.have.status(204);\r",
							"});"
						],
						"type": "text/javascript"
					}
				}
			],
			"request": {
				"auth": {
					"type": "bearer",
					"bearer": [
						{
							"key": "token",
							"value": "{{accessToken}}",
							"type": "string"
						}
					]
				},
				"method": "PATCH",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n  \"customerName\": \"John {{$randomFullName}}\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "{{baseUrl}}/orders/:orderId",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"orders",
						":orderId"
					],
					"variable": [
						{
							"key": "orderId",
							"value": "{{orderId}}"
						}
					]
				}
			},
			"response": []
		},
		{
			"name": "Delete order",
			"event": [
				{
					"listen": "test",
					"script": {
						"exec": [
							"pm.test(\"Status code is 204\", () => {\r",
							"    pm.response.to.have.status(204);\r",
							"});\r",
							"\r",
							"postman.setNextRequest(null);"
						],
						"type": "text/javascript"
					}
				}
			],
			"request": {
				"auth": {
					"type": "bearer",
					"bearer": [
						{
							"key": "token",
							"value": "{{accessToken}}",
							"type": "string"
						}
					]
				},
				"method": "DELETE",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n  \"customerName\": \"John {{$randomFullName}}\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "{{baseUrl}}/orders/:orderId",
					"host": [
						"{{baseUrl}}"
					],
					"path": [
						"orders",
						":orderId"
					],
					"variable": [
						{
							"key": "orderId",
							"value": "{{orderId}}"
						}
					]
				}
			},
			"response": []
		}
	],
	"event": [
		{
			"listen": "prerequest",
			"script": {
				"type": "text/javascript",
				"exec": [
					""
				]
			}
		},
		{
			"listen": "test",
			"script": {
				"type": "text/javascript",
				"exec": [
					""
				]
			}
		}
	],
	"variable": [
		{
			"key": "baseUrl",
			"value": "https://simple-books-api.glitch.me"
		},
		{
			"key": "accessToken",
			"value": "e65a84ff89476f084d603eba0aca3d004266dbd37fa04aec04d0219db831219a",
			"type": "string"
		}
	]
}