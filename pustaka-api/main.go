package main

import (
	"pustaka-api/handler"
	"pustaka-api/repo"
	"pustaka-api/service"

	"github.com/gin-gonic/gin"
)

func main() {

	repoRepository := repo.NewRepository(db)
	//repoFileRepository := repo.NewFileRepository()
	serviceService := service.NewService(repoRepository)
	bookHandler := handler.NewBookHandler(serviceService)

	//serviceService.Create(bookInput)

	router := gin.Default()

	v1 := router.Group("/v1")

	//GET
	/*v1.GET("/", bookHandler.RootHandler)
	v1.GET("/hello", bookHandler.HelloHandler)
	v1.GET("/books/:id/:title", bookHandler.BooksHandler)
	v1.GET("/books/query", bookHandler.BooksQueryHandler)*/

	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBook)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)

	v1.GET("/store/:id", handler.StoreHandler)
	v1.GET("/store/query", handler.StoreQueryHandler)
	v1.POST("/store", handler.PostStoreHandler)

	router.Run(":8888")
}
