package book

import "go.mongodb.org/mongo-driver/bson/primitive"

type BookResponse struct {
	IdBook      primitive.ObjectID `json:"id_book" bson:"id_book"`
	Title       string             `json:"title" bson:"book_title"`
	Description string             `json:"description" bson:"book_description"`
	Author      string             `json:"author" bson:"book_author"`
	Price       int                `json:"price" bson:"book_store"`
	Rating      int                `json:"rating" bson:"book_rating"`
	Discount    int                `json:"discount" bson:"book_discount"`
}
