package book

import "time"

type BookInput struct {
	Title       string    `json:"title" bson:"book_title"`
	Description string    `json:"description" bson:"book_description"`
	Author      string    `json:"author" bson:"book_author"`
	Price       int       `json:"price" bson:"book_store"`
	Rating      int       `json:"rating" bson:"book_rating"`
	Discount    int       `json:"discount" bson:"book_discount"`
	CreatedAt   time.Time `json:"create" bson:"book_create"`
	UpdatedAt   time.Time `json:"update" bson:"book_update"`
	StoreName   string    `json:"name" bson:"store_name"`
}

type StoreInput struct {
	StoreName string `json:"name" bson:"store_name"`
	Address   string `json:"address" bson:"store_address"`
	State     string `json:"state" bson:"state"`
	City      string `json:"city" bson:"city"`
}
