package book

import "go.mongodb.org/mongo-driver/bson/primitive"

type Relation struct {
	IdBook  primitive.ObjectID `json:"id_book" bson:"id_book"`
	IdStore primitive.ObjectID `json:"id_store" bson:"id_store"`
}
