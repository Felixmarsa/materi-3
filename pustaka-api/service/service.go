package service

import (
	"pustaka-api/book"
	"pustaka-api/repo"
)

type Service interface {
	FindAll() ([]book.Book, error)
	FindByID(IdBook int) (book.Book, error)
	Create(book book.BookInput) (book.Book, error)
	Update(IdBook int, book book.BookInput) (book.Book, error)
	Delete(IdBook int) (book.Book, error)
}

type service struct {
	repository repo.Repository
}

func NewService(repository repo.Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]book.Book, error) {
	books, err := s.repository.FindAll()
	return books, err
}

func (s *service) FindByID(IdBook int) (book.Book, error) {
	book, err := s.repository.FindByID(IdBook)
	return book, err
}

func (s *service) Create(bookInput book.BookInput) (book.Book, error) {

	book := book.Book{
		Title:       bookInput.Title,
		Description: bookInput.Description,
		Author:      bookInput.Author,
		Price:       bookInput.Price,
		Rating:      bookInput.Rating,
		Discount:    bookInput.Discount,
	}
	newBook, err := s.repository.Create(book)
	return newBook, err
}

func (s *service) Update(IdBook int, bookInput book.BookInput) (book.Book, error) {
	book, err := s.repository.FindByID(IdBook)

	book.Title = bookInput.Title
	book.Description = bookInput.Description
	book.Author = bookInput.Author
	book.Price = bookInput.Price
	book.Rating = bookInput.Rating
	book.Discount = bookInput.Discount

	newBook, err := s.repository.Update(book)
	return newBook, err
}

func (s *service) Delete(IdBook int) (book.Book, error) {
	book, err := s.repository.FindByID(IdBook)

	newBook, err := s.repository.Delete(book)
	return newBook, err
}
