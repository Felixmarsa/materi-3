package repo

import (
	"errors"
	"fmt"
	"pustaka-api/book"
)

type fileRepository struct {
}

func NewFileRepository() *fileRepository {
	return &fileRepository{}
}

func (r *fileRepository) FindAll() ([]book.Book, error) {
	var books []book.Book

	fmt.Println("FindAll")

	return books, errors.New("dummy")
}

func (r *fileRepository) FindByID(IdBook int) (book.Book, error) {
	var book book.Book

	fmt.Println("FindByID")

	return book, nil
}

func (r *fileRepository) Create(book book.Book) (book.Book, error) {
	fmt.Println("Create")

	return book, nil
}
