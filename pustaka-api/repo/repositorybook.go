package repo

import (
	"pustaka-api/book"

	"gorm.io/gorm"
)

type Repository interface {
	FindAll() ([]book.Book, error)
	FindByID(IdBook int) (book.Book, error)
	Create(book book.Book) (book.Book, error)
	Update(book book.Book) (book.Book, error)
	Delete(book book.Book) (book.Book, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func new(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]book.Book, error) {
	var books []book.Book

	err := r.db.Find(&books).Error

	return books, err
}

func (r *repository) FindByID(IdBook int) (book.Book, error) {
	var book book.Book

	err := r.db.Find(&book, IdBook).Error

	return book, err
}

func (r *repository) Create(book book.Book) (book.Book, error) {
	err := r.db.Create(&book).Error

	return book, err
}

func (r *repository) Update(book book.Book) (book.Book, error) {
	err := r.db.Save(&book).Error
	return book, err
}

func (r *repository) Delete(book book.Book) (book.Book, error) {
	err := r.db.Delete(&book).Error
	return book, err
}
