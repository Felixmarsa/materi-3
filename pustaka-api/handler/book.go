package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	"pustaka-api/book"
	"pustaka-api/service"
)

type bookHandler struct {
	serviceService service.Service
}

//Handler
func NewBookHandler(serviceService service.Service) *bookHandler {
	return &bookHandler{serviceService}
}

func (h *bookHandler) GetBooks(c *gin.Context) {
	books, err := h.serviceService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	var booksResponse []book.BookResponse

	for _, b := range books {
		bookResponse := convertToBookResponse(b)

		booksResponse = append(booksResponse, bookResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": booksResponse,
	})
}

func (h *bookHandler) GetBook(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	b, err := h.serviceService.FindByID(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	bookResponse := convertToBookResponse(b)

	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
	})
}

/*
func (h *bookHandler) RootHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"name": "Felix Marsaptandhiyo",
		"bio":  "Backend Engineer intern",
	})
}

func (h *bookHandler) HelloHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"title":    "Hello World",
		"subtitle": "Belajar Golang bareng Felix Marsaptandhiyo",
	})
}

func (h *bookHandler) BooksHandler(c *gin.Context) {
	id := c.Param("id_book")
	title := c.Param("title")
	description := c.Param("description")
	author := c.Param("author")
	price := c.Param("price")
	rating := c.Param("rating")
	discount := c.Param("discount")
	create := c.Param("create")
	update := c.Param("update")

	c.JSON(http.StatusOK, gin.H{"id_book": id, "title": title, "description": description, "author": author, "price": price,
		"rating": rating, "discount": discount, "create": create, "update": update})
}

//Path Variable dan Query String
func (h *bookHandler) BooksQueryHandler(c *gin.Context) {
	title := c.Query("title")
	description := c.Query("description")
	author := c.Query("author")
	price := c.Query("price")
	rating := c.Query("rating")
	discount := c.Query("discount")
	create := c.Query("create")
	update := c.Query("update")

	c.JSON(http.StatusOK, gin.H{"title": title, "description": description, "author": author, "price": price,
		"rating": rating, "discount": discount, "create": create, "update": update})
}

*/

func (h *bookHandler) CreateBook(c *gin.Context) {
	var bookInput book.BookInput

	err := c.ShouldBindJSON(&bookInput)
	if err != nil {

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errorMessages,
		})
		return
	}

	book, err := h.serviceService.Create(bookInput)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
		//"title":       bookInput.Title,
		//"description": bookInput.Description,
		//"author":      bookInput.Author,
		//"price":       bookInput.Price,
		//"rating":      bookInput.Rating,
		//"discount":    bookInput.Discount,
		//"create":      bookInput.CreatedAt,
		//"update":      bookInput.UpdatedAt,
	})

}

func (h *bookHandler) UpdateBook(c *gin.Context) {
	var bookInput book.BookInput

	err := c.ShouldBindJSON(&bookInput)
	if err != nil {

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errorMessages,
		})
		return
	}

	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	book, err := h.serviceService.Update(id, bookInput)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
		//"title":       bookInput.Title,
		//"description": bookInput.Description,
		//"author":      bookInput.Author,
		//"price":       bookInput.Price,
		//"rating":      bookInput.Rating,
		//"discount":    bookInput.Discount,
		//"create":      bookInput.CreatedAt,
		//"update":      bookInput.UpdatedAt,
	})

}

func (h *bookHandler) DeleteBook(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	book, err := h.serviceService.Delete(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToBookResponse(book),
	})

}

func convertToBookResponse(b book.Book) book.BookResponse {
	return book.BookResponse{
		IdBook:      b.IdBook,
		Title:       b.Title,
		Description: b.Description,
		Author:      b.Author,
		Price:       b.Price,
		Rating:      b.Rating,
		Discount:    b.Discount,
	}
}
