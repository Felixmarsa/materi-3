package handler

import (
	"fmt"
	"net/http"
	"pustaka-api/book"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

func StoreHandler(c *gin.Context) {
	id := c.Param("id_store")
	name := c.Param("name")
	address := c.Param("address")
	state := c.Param("state")
	city := c.Param("city")

	c.JSON(http.StatusOK, gin.H{"id_store": id, "name": name, "address": address, "state": state, "city": city})
}

//Path Variable dan Query String
func StoreQueryHandler(c *gin.Context) {
	name := c.Query("name")
	address := c.Query("address")
	state := c.Query("state")
	city := c.Query("city")

	c.JSON(http.StatusOK, gin.H{"name": name, "address": address, "state": state, "city": city})
}

func PostStoreHandler(c *gin.Context) {
	var storeInput book.StoreInput

	err := c.ShouldBindJSON(&storeInput)
	if err != nil {

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errorMessages,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"name":    storeInput.StoreName,
		"address": storeInput.Address,
		"state":   storeInput.State,
		"city":    storeInput.City,
	})

}
