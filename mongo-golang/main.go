package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"

	"github.com/felixmarsa/mongo-golang/controllers"
)

func main() {

	r := httprouter.New()
	uc := controllers.NewBooksControllers(getSession())
	r.GET("/book/:id", uc.GetBook)
	r.POST("/book", uc.CreateBook)
	r.DELETE("/book/:id", uc.DeleteBook)
	http.ListenAndServe("localhost:9000", r)
}

func getSession() *mgo.Session {

	s, err := mgo.Dial("mongodb://localhost:27107")
	if err != nil {
		panic(err)
	}
	return s
}
