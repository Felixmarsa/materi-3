package models

import "gopkg.in/mgo.v2/bson"

type Book struct {
	IdBook      bson.ObjectId `json:"id" bson:"_id"`
	Title       string        `json:"title" bson:"title"`
	Price       int           `json:"price" bson:"price"`
	Author      string        `json:"author" bson:"author"`
	Description string        `json:"description" bson:"description"`
}
